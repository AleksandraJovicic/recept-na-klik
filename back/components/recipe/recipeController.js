const mongoose = require('mongoose');
const fs = require('fs');

const Recipe = require('./recipeModel');

const keysRecipe = require('../../database/recipes.json');

module.exports.getRecipes = async function (req, res, next) {
  try {
    var recipes;
    var query = Recipe.find({});
    if (req.query.userId){
      const userId = req.query.userId;
      query = query.where('userId').equals(userId);
     }
     if (req.query.ingredients){
      const ingredients = req.query.ingredients;
      query = query.where('ingredients').in(ingredients);
    }
    if(req.query.difficulty){
      const difficultyString = req.query.difficulty;
      const difficulty = JSON.parse(difficultyString);
      var gte = 0;
      var lte = 10;
      if ("gt" in difficulty){
        gte = difficulty["gt"];
      }
      if ("lt" in difficulty){
        lte = difficulty["lt"];
      }
      query = query.where('difficulty').gte(gte).lte(lte);
    }
    if (req.query.title){
      const title = req.query.title;
      query = query.where('title').equals(title);
    }
    if (req.query.preptime){
      const preptimeString = req.query.preptime;
      const preptime = JSON.parse(preptimeString);
      var gte = 0;
      var lte = 24*60;
      if ("gt" in preptime){
        gte = preptime["gt"];
      }
      if ("lt" in preptime){
        lte = preptime["lt"];
      }
      query = query.where('preptime').gte(gte).lte(lte);
    }
    if (req.query.category){
      const category = req.query.category;
      query = query.where('category').in(category);
    }
    if (req.query.createdAt) {
      const createdAt = req.query.createdAt;
      query = query.where('createdAt').equals(createdAt);
    }
    if (req.query.page && req.query.limit){
      const page = parseInt(req.query.page);
      const limit = parseInt(req.query.limit);
      query = query.skip((page - 1) * limit).limit(page * limit);
    }
    if (req.query.score){
      const score = req.query.score;
      query = query.find({$where: "this.scores.scoreSum / this.scores.scoreNumber > " + score});
    }
    await Recipe.updateMany({}, [{ $set: {"scores.scoreAvg": {$divide: ["$scores.scoreSum", "$scores.scoreNumber"]}}}], {multi: true}).exec();
    recipes = await query.sort('-scores.scoreAvg').exec();
    res.status(200).json(recipes);
  } catch (err) {
    next(err);
  }
};



module.exports.createRecipe = async function (req, res, next) {
  const recipeObject = {
    _id: new mongoose.Types.ObjectId(),
    userId: req.body.userId,
    title: req.body.title,
    difficulty: req.body.difficulty,
    preptime: req.body.preptime,
    course: req.body.course,
    category: req.body.category.split(','),
    createdAt: new Date().toISOString().slice(0,10),
    updates: [{"date": new Date().toISOString().slice(0,10), "action": "created"}],
    instructions: req.body.instructions.split("\r\n"),
    serving: req.body.serving,
    units: req.body.units,
    scores: {"users":[]},
    ingredients: req.body.ingredients.split(','),
    photo: {"data": fs.readFileSync(req.file.path), "contentType": 'image/jpeg'}
  };

  const recipe = new Recipe(recipeObject);

  try {
    const savedRecipe = await recipe.save();
    res.status(201).json({
      message: 'A recipe is successfully created',
      recipe: savedRecipe,
    });
  } catch (err) {
    next(err);
  }
};

module.exports.getByRecipeId = async function (req, res, next) {
  const recipeId = req.params.recipeId;

  try {
    var recipe = await  Recipe.findById(recipeId).exec();

    if (!recipe) {
      return res
        .status(404)
        .json({ message: 'The recipe with given id does not exist' });
    }
    if(recipe.scores["scoreNumber"]){
      console.log("ej");
      await Recipe.updateOne({_id: recipeId}, { $set: {"scores.scoreAvg": {$divide: ["$scores.scoreSum", "$scores.scoreNumber"]}}}).exec();
      recipe = await  Recipe.findById(recipeId).exec();
    }

    recipe.createdAt = recipe.createdAt.toString().slice(0,10);
    res.status(200).json(recipe);
  } catch (err) {
    next(err);
  }
};

module.exports.updateByRecipeId = async function (req, res, next) {
  const recipeId = req.params.recipeId;
  const updateObject = req.body;
  updateObject['category'] = updateObject['category'].split(',').filter(word => word != "");
  updateObject['instructions'] = updateObject['instructions'].split("\r\n").filter(word => word != "");
  updateObject['ingredients'] = updateObject['ingredients'].split(',').filter(word => word != "");


  var validationMessages = {};
  var updateChanges = [];
  var i = 1;
  Object.keys(updateObject).forEach(function(key) {
    if (!keysRecipe.hasOwnProperty(key)) {                
        validationMessages["validationError" + i] = "Key " + key + " doesn't exist and it wasn't updated.";
        i++;
    } else {
        updateChanges.push({ "date": new Date().toISOString().slice(0,10), "action" : key + " changed to " + updateObject[key]});
    }
  });
  if (req.file!=undefined) {
    updateObject["photo"] = {"data": fs.readFileSync(req.file.path), "contentType": 'image/jpeg'};
    updateChanges.push({ "date": new Date().toISOString().slice(0,10), "action" : "photo was changed" });
  }

  if (Object.keys(validationMessages).length == 0) {
      validationMessages["errors"] = "There was no validation errors.";
  }

  try {
    await Recipe.updateOne({ _id: recipeId }, { $set: updateObject }).exec();
    await Recipe.updateOne({ _id: recipeId }, { $push: { updates: updateChanges }}).exec();
    res.status(200).json([validationMessages, { message: 'The recipe is successfully updated' }]);
  } catch (err) {
    next(err);
  }
};

module.exports.deleteByRecipeId = async function (req, res, next) {
  const recipeId = req.params.recipeId;

  try {
    await Recipe.deleteOne({ _id: recipeId }).exec();
    res.status(200).json({ message: 'The recipe is successfully deleted' });
  } catch (err) {
    next(err);
  }
};

module.exports.giveScoreOrCommentToRecipe = async function (req, res, next) {
  const recipeId = req.params.recipeId;
  const userId = req.params.userId;
  const updateObject = req.body;
  try {
    const stringScoreOrComment = req.query.scoreOrComment;
    if (stringScoreOrComment === "comment"){
      const comment = 
      await Recipe.updateOne({ _id: recipeId }, { $push: {comments: {userId: userId, comment: updateObject["comment"]}}}).exec();
      res.status(200).json({ message: 'The recipe is successfully commented' });
    }
    else if(stringScoreOrComment === "score"){
      const recipe = await Recipe.findById(recipeId);
      if(!(recipe.get("scores").get("users").includes(userId))){
        await Recipe.updateOne({ _id: recipeId }, { $inc: {"scores.scoreNumber": 1, "scores.scoreSum": updateObject["score"]}, $push: {"scores.users": userId}}).exec();
        res.status(200).json({ message: 'The recipe is successfully rated' });
      }
      else{
        res.status(200).json({ message: 'The recipe is already rated by you' });
      }
    }
  } catch (err) {
    next(err);
  }
};