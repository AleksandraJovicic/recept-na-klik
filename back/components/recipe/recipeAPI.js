const express = require('express');

const router = express.Router();

const controller = require('./recipeController');

var multer  =   require('multer');
const path = require('path')

var storage =   multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, '../recipe_photos/'));
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + '.jpg');
  }
});
var upload = multer({ storage : storage}).single('photo');

router.get('/', controller.getRecipes);
router.post('/', upload, controller.createRecipe);
router.get('/:recipeId', controller.getByRecipeId);
router.put('/:recipeId/:userId', controller.giveScoreOrCommentToRecipe);
router.put('/:recipeId', upload, controller.updateByRecipeId);
router.delete('/:recipeId', controller.deleteByRecipeId);

module.exports = router;