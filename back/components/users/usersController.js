const mongoose = require('mongoose');
const fs = require('fs');

const User = require('./usersModel');
const Recipe = require('../recipe/recipeModel');

const keysUser = require('../../database/users.json');

module.exports.getUsers = async function(req, res, next) {
    try {
        var query = User.find({});
        if (req.query.firstName) {
            const firstName = req.query.firstName
            query = query.where('firstName').equals(firstName);
        }
        if (req.query.lastName) {
            const lastName = req.query.lastName;
            query = query.where('lastName').equals(lastName);
        }
        if (req.query.createdAt) {
            const createdAt = req.query.createdAt;
            query = query.where('createdAt').equals(createdAt);
        }
        if (req.query.favorites) {
            const favorites = req.query.favorites;
            query = query.where('favorites').in(favorites);
        }
        if (req.query.username) {
            const username = req.query.username;
            query = query.where('username').equals(username);
        }
        if (req.query.password) {
            const password = req.query.password;
            query = query.where('password').equals(password);
        }
        if (req.query.page && req.query.limit){
            const page = parseInt(req.query.page);
            const limit = parseInt(req.query.limit);
            query = query.skip((page - 1) * limit).limit(page * limit);
        }
        const users = await query.sort('username').exec();
        res.status(200).json(users);
    } catch (err) {
        next(err);
    }
};

module.exports.getByUserId = async function(req, res, next) {
    const userId = req.params.userId;

    try {
        const user = await User.findById(userId).exec();

        if (user) {
            res.status(200).json(user);
        } else {
            return res.status(404).json({ message: "User with specified id does not exist." });
        }
    } catch(err) {
        next(err);
    }
};

module.exports.getFavoritesByUserId = async function(req, res, next) {
    const userId = req.params.userId;

    try {
        const user = await User.findById(userId).exec();
        if (user) {
            res.status(200).json(user.favorites);
        } else {
            return res.status(404).json({ message: "User with specified id does not exist." });
        }
    } catch(err) {
        next(err);
    }
};

module.exports.createUser = async function(req, res, next) {
    const user = new User({
        _id: new mongoose.Types.ObjectId,
        username: req.body.username,
        password: req.body.password,
        email: req.body.email,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        createdAt: new Date().toISOString().slice(0,10),
        updates: [{"date": new Date().toISOString().slice(0,10), "action": "created"}],
        photo: {"data": fs.readFileSync(req.file.path), "contentType": 'image/jpeg'}
    });

    try {
        const savedUser = await user.save();
        res.status(201).json(savedUser);
    } catch(err) {
        next(err);
    }
};


module.exports.updateByUserId = async function(req, res, next) {
    const userId = req.params.userId;
    const updateObject = req.body;
    const user = await User.findById(userId).exec();

    var validationMessages = {};
    var updateChanges = [];
    var i = 1;
    Object.keys(updateObject).forEach(function(key) {
        if (!keysUser.hasOwnProperty(key)) {                
            validationMessages["validationError" + i] = "Key " + key + " doesn't exist and it wasn't updated.";
            i++;
        } else {
            if (key == "favorites") {
                if (user.favorites)
                    updateObject[key] = (updateObject[key] + ',' + user.favorites).split(',');
                else if (updateObject[key].length > 24)
                    updateObject[key] = (updateObject[key]).split(',');
            }
            if (key == "photo") {
                updateObject[key] = {"data": fs.readFileSync(req.file.path), "contentType": 'image/jpeg'};
            }
            updateChanges.push({ "date": new Date().toISOString().slice(0,10), "action" : key + " changed to " + updateObject[key]});
        }
    });
    if (req.file && req.file.path) {
        updateObject["photo"] = {"data": fs.readFileSync(req.file.path), "contentType": 'image/jpeg'};
        updateChanges.push({ "date": new Date().toISOString().slice(0,10), "action" : "photo was changed" });
    }

    if (Object.keys(validationMessages).length == 0) {
        validationMessages["errors"] = "There was no validation errors.";
    }
    try {
        await User.updateOne({ _id: userId }, { $set: updateObject }).exec();
        await User.updateOne({ _id: userId }, { $push: { updates: updateChanges }}).exec();
        res.status(200).json([validationMessages, {message: "Specified user was successfully updated."}]);
    } catch(err) {
        next(err);
    }
};

module.exports.deleteByUserId = async function(req, res, next) {
    const userId = req.params.userId;

    try {
        await User.deleteOne({ _id: userId }).exec();
        await Recipe.deleteMany({ userId: userId}).exec();
        res.status(200).json({message: "Specified user was successfully deleted."});
    } catch(err) {
        next(err);
    }
};

module.exports.deleteFavorite = async function(req, res, next) {
    const userId = req.params.userId;
    const recipeId = req.params.recipeId;

    try {
        await User.updateOne({_id: userId}, {$pull: {favorites: recipeId }}).exec();
        res.status(200).json({message: "Favorite was successfully deleted."})
    } catch(err) {
        next(err);
    }
};