const express = require('express');

const router = express.Router();

router.get('/', function (req, res, next) {
  const api = [
      { path: '/', children: [] },
      { path: '/recipe', children: [
          { path: '/', method: 'GET', children: [] },
          { path: '/', method: 'POST', children: [] },
          { path: '/', method: 'GET', parameters: ['recipeId'], children: [] },
          { path: '/', method: 'DELETE', parameters: ['recipeId'], children: [] },
          { path: '/', method: 'PUT', parameters: ['recipeId'], children: [] }
      ] },
      { path: '/user', children: [
          { path: '/', method: 'GET', children: [] },
          { path: '/', method: 'GET', parameters: ['userId'], children: [] },
          { path: '/', method: 'POST', children: [] },
          { path: '/', method: 'PUT', parameters: ['userId'], children: [] },
          { path: '/', method: 'DELETE', parameters: ['userId'], children: [] }
      ]}
  ];
  res.status(200).json(api);
});

module.exports = router;