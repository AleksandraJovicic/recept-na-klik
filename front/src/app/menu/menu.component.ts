import { Component, OnInit } from '@angular/core';
import { RecipeService } from '../services/recipe/recipe.service';
import { Router } from '@angular/router';
import { RecipeModel } from '../services/recipe/recipe.model';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private recipeSevice : RecipeService, private router : Router, public loginService: LoginService) { }

  ngOnInit(): void {
  }

  public toFilterPage(filterString){
    this.recipeSevice.refreshRecipes("?category[]=" + filterString);
    this.router.navigate(['findRecipes']);
  }
}