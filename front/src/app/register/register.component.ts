import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private router: Router, public loginService: LoginService) { }

  ngOnInit(): void {
  }

  public goToRegistration()
  {
    this.router.navigate(['registration'])
  }

}
