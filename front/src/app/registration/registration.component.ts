import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../services/user/user.service';
import { User } from '../services/user/user.model'
import { Router } from '@angular/router';



@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  
  public SignUpForm: FormGroup;
  public equalpassword: boolean=false;
  public fileToUpload: File;
  constructor(private formBuilder: FormBuilder,
     private userService: UserService,
     private router: Router 
) { 
  this.SignUpForm = formBuilder.group({      
    firstName:    ['', [Validators.required]],
    lastName:    ['', [Validators.required]],
    email:    ['', [Validators.required, Validators.email]],
    username: ['', [Validators.required]],
    password: ['', [Validators.required]],
    confirm:[''],
    photo:[]
  });


}
public submitForm(data): void {
  if (!this.SignUpForm.valid) {
    window.alert('Forma za kreiranje korisnika nije validna!');
    return;

  }

  this.userService.addUser(this.toFormData(this.SignUpForm.value))
    .subscribe((user: User) => {
      window.alert('Korisnik je uspesno napravljen!');
      console.log(user);
      this.SignUpForm.reset();
      this.router.navigate(['/']);
    });
}
  ngOnInit(): void {
  }

  fileChangeEvent(fileInput: any) {
    this.fileToUpload = <File>fileInput.target.files[0];
    this.SignUpForm.get('photo').setValue(this.fileToUpload);
  }
 
  public toFormData (formValue) {
    const formData = new FormData();
  
    for ( const key of Object.keys(formValue) ) {
      const value = formValue[key];
      formData.append(key, value);
    }
  formData.delete('confirm');
    return formData;
  }
  public get email(){
    return this.SignUpForm.get('email');
  }
  public get username(){
    return this.SignUpForm.get('username');
  }
  public get password(){
    return this.SignUpForm.get('password');
  }

  public get firstName(){
    return this.SignUpForm.get('firstName');
  }

  public get lastName(){
    return this.SignUpForm.get('lastName');
  }
  public get confirm(){
    return this.SignUpForm.get('confirm');
  }
  public equalpass(){
   if(this.password.value==this.confirm.value)
      this.equalpassword=false;
      else
      this.equalpassword=true;
  return this.equalpassword;
  }

  public getemailerr(){
    return this.SignUpForm.get('email').errors;
  }
  public getusernameerr(){
    return this.SignUpForm.get('username').errors;
  }
  public getpassworderr(){
    return this.SignUpForm.get('password').errors;
  }

  public getfirstNameerr(){
    return this.SignUpForm.get('firstName').errors;
  }

  public getlastNameerr(){
    return this.SignUpForm.get('lastName').errors;
  }

  public goToLogin()
  {
    this.router.navigate(['login']);
  }
}
