export interface RecipeModel {
    _id : string;
    userId : string;
    title : string;
    difficulty : number;
    preptime : number;
    course : string;
    category : string[];
    createdAt : string;
    updates : Array<Map<string, string>>;
    instructions : string[];
    serving : string;
    photo : File;
    units : number;
    ingredients : string[];
    scores : Map<string, string>;
    comments : Array<Map<string, string>>;
}
