import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RecipeModel } from './recipe.model';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { HttpErrorHandler } from 'src/app/utils/http-error-handler.model';
import { catchError, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RecipeService extends HttpErrorHandler {

  private recipes: Observable<RecipeModel[]>;

  private readonly recipeUrl = "http://localhost:3000/recipe/";

  constructor(private http : HttpClient, router: Router) {
    super(router);
    this.refreshRecipes("");
  }


  public createRecipe(formData, userId) : Observable<RecipeModel> {
    formData.append('userId', userId);
    return this.http.post<RecipeModel>(this.recipeUrl, formData).pipe(catchError(super.handleError()));;
  }

  public getRecipeById(id: string): Observable<RecipeModel> {
    return this.http.get<RecipeModel>(this.recipeUrl + id).pipe(catchError(super.handleError()));
  }

  public refreshRecipes(filterString): Observable<RecipeModel[]> {
    if (filterString == ""){
      this.recipes = this.http.get<RecipeModel[]>(this.recipeUrl);
    }
    else {
      this.recipes = this.http.get<RecipeModel[]>(this.recipeUrl.slice(0, -1) + filterString);
    }
    return this.recipes;
  }

  public getRecipes(): Observable<RecipeModel[]>{
    return this.recipes;
  }

  public deleteRecipe(id: string): Observable<RecipeModel[]> {
    return this.http.delete(this.recipeUrl + id).pipe(
      catchError(super.handleError()),
      switchMap(() => this.refreshRecipes(""))
    );
  }

  public updateRecipe(data, id : string){
    return this.http.put(this.recipeUrl + id, data).pipe(catchError(super.handleError()));
  }

  public giveScoreOrCommentToRecipe(data, recipeId, userId, info) {
    return this.http.put(this.recipeUrl + recipeId + '/' + userId + '?scoreOrComment=' + info, data).pipe(catchError(super.handleError()));
  }

}
