import { Injectable } from '@angular/core';
import { UserService } from './user/user.service';
import { User } from './user/user.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService  {
  private loggedIn = false;
  private loggedInUsername: string;
  private loggedInUserId: string;
  constructor(private userService: UserService) { }

  public getLoggedIn() {
    return this.loggedIn;
  }

  public getLoggedInUsername() {
    return this.loggedInUsername;
  }

  public getLoggedInUserId() {
    return this.loggedInUserId;
  }

  public logIn(user: User) {
    this.loggedInUsername = user.username;
    this.loggedInUserId = user._id;
    this.loggedIn = true;
  }

  public logOut() {
    this.loggedIn = false;
    this.loggedInUsername = "";
    this.loggedInUserId = "";
  }
}
