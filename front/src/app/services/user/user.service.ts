import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { User } from '../user/user.model';
import { HttpErrorHandler } from '../../utils/http-error-handler.model';
import { RecipeModel } from '../recipe/recipe.model';

@Injectable({
  providedIn: 'root',
})
export class UserService extends HttpErrorHandler {
  private users: Observable<User[]>;
  private readonly userUrl = 'http://localhost:3000/user/';
  private readonly recipesUrl = 'http://localhost:3000/recipe';

  constructor(private http: HttpClient, router: Router) {
    super(router);
    this.refreshUsers("");
  }

  public refreshUsers(filterString: string): Observable<User[]> {
    if (filterString == ""){
      this.users = this.http.get<User[]>(this.userUrl);
    }
    else {
      this.users = this.http.get<User[]>(this.userUrl.slice(0, -1) + filterString);
    }
    return this.users;
  }

  public getUsers(): Observable<User[]> {
    return this.users;
  }

  public getUserById(id: string): Observable<User> {
    return this.http
      .get<User>(this.userUrl + id)
      .pipe(catchError(super.handleError()));
  }

  public addUser(data) {
    return this.http
      .post<User>(this.userUrl, data)
      .pipe(catchError(super.handleError()));
  }

  public getRecipesForUser(id: string): Observable<RecipeModel[]> {
    return this.http
      .get<RecipeModel[]>(this.recipesUrl + '?userId=' + id)
      .pipe(catchError(super.handleError()));
  }

  public getFavoritesForUser(id: string) {
    return this.http
      .get<string[]>(this.userUrl + id + '/favorites')
      .pipe(catchError(super.handleError()));
  }

  public updateUser(data, id: string){
    return this.http.put(this.userUrl + id, data).pipe(catchError(super.handleError()));
  }

  public deleteFavorite(userId: string, recipeId: string) {
    return this.http.delete(this.userUrl + userId + '/favorites/' + recipeId).pipe(catchError(super.handleError()));
  }

}
