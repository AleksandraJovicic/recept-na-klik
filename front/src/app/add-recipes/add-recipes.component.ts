import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms'
import { _ParseAST } from '@angular/compiler';
import * as _ from "lodash";
import { RecipeService } from '../services/recipe/recipe.service';
import { RecipeModel } from '../services/recipe/recipe.model';
import { element } from 'protractor';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-add-recipes',
  templateUrl: './add-recipes.component.html',
  styleUrls: ['./add-recipes.component.css']
})
export class AddRecipesComponent implements OnInit {
  public userId: string;
  public fileToUpload: File;
  public checkoutForm : FormGroup;
  constructor(private formBuilder: FormBuilder, private recipeService: RecipeService, private router: Router, private route: ActivatedRoute) { 
    this.checkoutForm = this.formBuilder.group({
      title : ['',[Validators.required]],
      difficulty : ['', [Validators.required, Validators.max(10), Validators.min(1)]],
      preptime : ['', [Validators.required, Validators.max(180), Validators.min(5)]],
      category : this.formBuilder.array([]),
      ingredients : this.formBuilder.array([]),
      instructions : ['', [Validators.required]],
      course : ['',[Validators.required, Validators.pattern('((d|D)oru(c|č)ak)|((r|R)u(c|č)ak)|((v|V)e(c|č)era)')]],
      units : ['',[Validators.required]],
      serving : [],
      photo : ['',[Validators.required]]
    });
  }


  public getTitleErr(){
    return this.checkoutForm.get('title').errors;
  }

  public getDiffErr(){
    return this.checkoutForm.get('difficulty').errors;
  }

  public getTimeErr(){
    return this.checkoutForm.get('preptime').errors;
  }

  public getInstErr(){
    return this.checkoutForm.get('instructions').errors;
  }

  public getCoursErr(){
    return this.checkoutForm.get('course').errors;
  }

  public getUnitsErr(){
    return this.checkoutForm.get('units').errors;
  }

  public getPhotoErr(){
    return this.checkoutForm.get('photo').errors;
  }

  public addIngredient() {
    const ingrs = this.checkoutForm.controls.ingredients as FormArray;
    var c = this.formBuilder.control({});
    c.setValue('');
    ingrs.push(c);
  }

  public customTrackBy(index: number, obj: any): any {
    return index;
  }

  get category(){
    return this.checkoutForm.get('category') as FormArray;
  }

  public onCheckboxChange(e) {
    const category: FormArray = this.checkoutForm.get('category') as FormArray;
  
    if (e.target.checked) {
      category.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      category.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          category.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  fileChangeEvent(fileInput: any) {
    this.fileToUpload = <File>fileInput.target.files[0];
    this.checkoutForm.get('photo').setValue(this.fileToUpload);
  }

  public submitForm(data) {
    if (!this.checkoutForm.valid){
      window.alert("Forma nije validna!");
      return;
    }
    var cats = this.checkoutForm.controls.category as FormArray;
    var ingrs = this.checkoutForm.controls.ingredients as FormArray;

    if (cats.length == 0 ){
      window.alert("Unesite bar jednu kategoriju");
      return;
    }

    if (ingrs.length == 0 ){
      window.alert("Unesite bar jedan sastojak");
      return;
    }

    this.recipeService.createRecipe(this.toFormData(this.checkoutForm.value), this.userId).subscribe((recipe : RecipeModel) => {
      window.alert("Vaš recept je sačuvan!");
      const ingrs = this.checkoutForm.controls.ingredients as FormArray;
      while (ingrs.length !== 0) {
        ingrs.removeAt(0);
      }
      var cats = document.getElementsByClassName('form-check-input categoryCheckBox') as HTMLCollectionOf<HTMLInputElement>;
      for (var i = 0; i < cats.length; i++) { 
        cats[i].checked = false; 
      } 
      this.checkoutForm.reset();

      const catsControl = this.checkoutForm.controls.category as FormArray;
      while (catsControl.length !== 0) {
        catsControl.removeAt(0);
      }

      (document.getElementById("img") as HTMLInputElement).value = '';

    });
  }

  public toFormData (formValue) {
    const formData = new FormData();
  
    for ( const key of Object.keys(formValue) ) {
      const value = formValue[key];
      formData.append(key, value);
    }
  
    return formData;
  }

  ngOnInit(): void {
    this.route.url.subscribe(x => this.userId = x[0].path);
  }

}