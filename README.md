# Project 29-Recept-na-klik-

Recept na klik je veb aplikacija koja omogućava korisnicima da pretražuju različite recepte po specifičnostima koji su njima od značaja, kao i da dele sopstvene recepte. Moguće je pretražiti recepte po sastojcima, po vremenu ili težini pripreme, po najmanjoj oceni ili po nekoj od 12 vrsta kategorija. Takođe, aplikacija nudi mogućnost registrovanja, čime se postaje deo kulinarske zajednice, i na taj način pruža priliku korisnicima da čuvaju svoje omiljene recepte, da ih ocenjuju ili komentarišu i prvenstveno da sa ostalima podele svoje kulinarske tajne.

## Tehnički detalji

Za bazu je korišćen MongoDB sistem za upravljanje nerelacionim bazama podataka. Za serversku stranu korišćen
je Node.js sa upotrebom Express.js radnog okvira. Za klijentsku stranu korišćen je Angular9.

## Konfiguracija i način pokretanja

Potrebno je skinuti ceo projekat i uraditi sledeće instrukcije u terminalu.

### Angular 
```
cd front  
npm install  
cd ..  
```
### Node.js
```
cd back  
npm install  
cd ..  
```
### MongoDB
```
mongoimport --db receptNaKlik --collection recipes --file database/recipes.json --jsonArray  
mongoimport --db receptNaKlik --collection users --file database/users.json --jsonArray  
```
### Pokretanje
U direktorijumu front iskucati ```ng serve``` instrukciju, dok u direktorijumu back iskucati ```nodemon server.js``` ili ```node server.js``` instrukciju. Pokrenuti aplikaciju na http://localhost:4200


## Developers

- [Vladana Đorđević, 1092/2019](https://gitlab.com/VladanaDjordjevic)
- [Aleksandra Jovičić , 1088/2019](https://gitlab.com/AleksandraJovicic)
- [Tijana Tošev, 1101/2019](https://gitlab.com/tijanatosev)
- [Jovana Nikolić, 1106/2019](https://gitlab.com/nickjovana)
